<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_items', function (Blueprint $table) {
            $table->increments('supplieritem_id');
            $table->integer('supplierName');
            $table->string('itemName');
            $table->string('itemQty');
            $table->string('itemPrice');
            $table->string('priceAmount');
            $table->integer('store_branch');
            $table->date('arrivalDate');
            $table->date('paymentDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_items');
    }
}
