<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_items', function (Blueprint $table) {
            $table->increments('saleitem_id');
            $table->integer('sale_itemID');
            $table->integer('sale_itemqty');
            $table->string('sale_price');
            $table->string('sale_total');
            $table->string('saleno_code');
            $table->integer('saleitem_shop');
            $table->date('saleitem_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_items');
    }
}
