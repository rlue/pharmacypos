<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();




Route::get('/backend/login','Auth\LoginController@showLoginForm')->name('backend.login');
Route::post('/backend/login','Auth\LoginController@login');
Route::group(['middleware' => 'auth'], function () {
	Route::get("/", 'Admin\DashboardController@index');
	
	Route::get("users", 'Admin\UserController@index')->name("users.index");
	Route::get("users/create", 'Admin\UserController@create')->name("users.create");
	Route::get("users/{id}/edit", 'Admin\UserController@edit')->name("users.edit");
	Route::get("users/delete/{id}", 'Admin\UserController@destroy')->name("users.delete");
	Route::patch('users/update/{id}',"Admin\UserController@update")->name('users.update');
	Route::post('users/store', 'Admin\UserController@store')->name("users.store");

	Route::get("stores", 'Admin\StoreController@index')->name("stores.index");
	Route::get("stores/create", 'Admin\StoreController@create')->name("stores.create");
	Route::get("stores/{id}/edit", 'Admin\StoreController@edit')->name("stores.edit");
	Route::get("stores/delete/{id}", 'Admin\StoreController@destroy')->name("stores.delete");
	Route::patch('stores/update/{id}',"Admin\StoreController@update")->name('stores.update');
	Route::post('stores/store', 'Admin\StoreController@store')->name("stores.store");

	Route::get("item", 'Admin\ItemController@index')->name("item.index");
	Route::get("item/create", 'Admin\ItemController@create')->name("item.create");
	Route::get("item/{id}/edit", 'Admin\ItemController@edit')->name("item.edit");
	Route::get("item/delete/{id}", 'Admin\ItemController@destroy')->name("item.delete");
	Route::patch('item/update/{id}',"Admin\ItemController@update")->name('item.update');
	Route::post('item/store', 'Admin\ItemController@store')->name("item.store");

	Route::get("supplier", 'Admin\SupplierController@index')->name("supplier.index");
	Route::get("supplier/create", 'Admin\SupplierController@create')->name("supplier.create");
	Route::get("supplier/{id}/edit", 'Admin\SupplierController@edit')->name("supplier.edit");
	Route::get("supplier/delete/{id}", 'Admin\SupplierController@destroy')->name("supplier.delete");
	Route::patch('supplier/update/{id}',"Admin\SupplierController@update")->name('supplier.update');
	Route::post('supplier/store', 'Admin\SupplierController@store')->name("supplier.store");

	
	//Export Supplier Item
	Route::post("supplierItem/exportItem", 'Admin\SupplierItemController@downloadExcelFile')->name("supplierItem.export");
	//Supplier Item
	Route::get("supplierItem", 'Admin\SupplierItemController@index')->name("supplierItem.index");
	Route::get("supplierItem/create", 'Admin\SupplierItemController@create')->name("supplierItem.create");
	Route::get("supplierItem/{id}/edit", 'Admin\SupplierItemController@edit')->name("supplierItem.edit");
	Route::get("supplierItem/delete/{id}", 'Admin\SupplierItemController@destroy')->name("supplierItem.delete");
	Route::patch('supplierItem/update/{id}',"Admin\SupplierItemController@update")->name('supplierItem.update');
	Route::post('supplierItem/store', 'Admin\SupplierItemController@store')->name("supplierItem.store");

	//Sale Item
	Route::get("sale", 'Admin\SaleController@index')->name("sale.index");
	Route::get("sale/create", 'Admin\SaleController@create')->name("sale.create");
	Route::get("sale/{id}/edit", 'Admin\SaleController@edit')->name("sale.edit");
	Route::get("sale/delete/{id}", 'Admin\SaleController@destroy')->name("sale.delete");
	Route::patch('sale/update/{id}',"Admin\SaleController@update")->name('sale.update');
	Route::post('sale/store', 'Admin\SaleController@store')->name("sale.store");

	Route::get("sale/receive", 'Admin\SaleController@receive')->name("sale.receive");
	Route::post('sale/saledetail', 'Admin\SaleController@saledetail')->name("sale.saledetail");

	//get item form ajax
	Route::post('sale/CodeItem','Admin\SaleController@CodeItem')->name('codeitem');
	//check item qty has or not
	Route::post('sale/checkqty','Admin\SaleController@checkqty')->name('checkqty');
	//sal export
	Route::post('sale/itemexport', 'Admin\SaleController@SaleExport')->name("sale.itemexport");
	//Total export
	Route::post('sale/totalexport', 'Admin\SaleController@totalExport')->name("sale.totalexport");
});