<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Pharmacy | MYOB </title>

    <!-- Bootstrap -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
     
     <link href="{{ asset('css/nprogress.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
       <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">
       <link href="{{ asset('css/summernote.css') }}" rel="stylesheet">

       @yield('headerstyle')
  </head>

  <body class="nav-md">
    <div class="body">
      <div class="main_container">
        

              <nav class="navbar navbar-default">
      <div class="container">
        <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/">Pharmacy | MYOB</a>
          </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">အေရာင္း <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{ route('sale.index') }}">အေရာင္းစာရင္း</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="{{ route('sale.create') }}">ေရာင္းရန္</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ျဖန္႕ေဝသူ <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{ route('supplier.index') }}">ျဖန္႕ေဝသူ စီမံေရး</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ပစၥည္း <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{ route('item.index') }}">ပစၥည္း စီမံေရး</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="{{ route('supplierItem.index') }}">ျဖန္႕ေဝသူ၏ ပစၥည္း</a></li>
                    </ul>
                </li>
                <?php if(Auth::user()->is_super()){ ?> 
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ဆိုင္ခြဲမ်ား <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{ route('stores.index') }}">ဆိုင္ခြဲမ်ား</a></li>
                    </ul>
                </li>
                <?php } ?>
                 <?php if(Auth::user()->is_super()){ ?> 
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ဝန္ထမ္းမ်ား <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{ route('users.index') }}">ဝန္ထမ္း စီမံေရး</a></li>
                    </ul>
                </li>
                <?php } ?>
              </ul>
               
              <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
                    </ul>
                </li>
              </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </div>
  </nav>
        <!-- /top navigation -->

        <!-- page content -->
        <br>
        <div class="container">
          @yield('content')

        </div>

         
       
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Infologin Media Production by <a href="http://infologicart.com">infologicart.com</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->

      </div>
    </div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- jQuery -->

    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap-progressbar.min.js') }}"></script>
    <!-- FastClick -->

    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    
    <script src="{{ asset('js/nprogress.js') }}"></script>

    <!-- NProgress -->
   <script src="{{ asset('js/summernote.min.js') }}"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="{{ asset('js/custom.min.js') }}"></script>

    @yield('footerscript')
	<script type="text/javascript">
   
   $(document).ready(function() {
  $('#summernote').summernote();
});
 
  </script>
  
  </body>
</html>
