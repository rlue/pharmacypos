@extends('layouts.admin')

@section('headerstyle')
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@stop
@section('content')
    

   <div class="right_col" role="main">
          <div id="saleitem">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>အေရာင္းအသစ္တင္ရန္ေဖာင္ <small>different form elements</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    
    
                    

                    <form method="post" id="sale" action="{{ route('sale.store') }}">
                        {{ csrf_field() }}
                      <div class="row">
                         <div class="col-md-3">
                           <div class="form-group ">
                            <label for="sale_buyer">ဝယ္သူ အမည္</label>
                            <input type="text" class='form-control' name="sale_buyer" value="{{old('sale_buyer')}}" placeholder="ဝယ္သူ အမည္" required>
                            <label for="sale_seller">ေရာင္းသူ အမည္</label>
                            <input type="text" class='form-control' name="sale_seller" value="{{old('sale_seller')}}" placeholder="ေရာင္းသူ အမည္" required>
                             <input type="hidden" class='form-control' name="saleitem_shop" value="{{ Auth::user()->branch }}" >
                            
                        </div>
                         </div>
                         <div class="col-md-offset-6 col-md-3">
                         <div class="form-group ">
                            <label for="saleitem_date">ရက္စြဲ</label>
                            <input type="text" class='form-control' name="saleitem_date" value="<?php echo date('Y-m-d'); ?>" readonly required>
                             <label for="saleno_code">အေရာင္း နံပါတ္</label>
                            <input type="text" class='form-control' name="saleno_code" value="{{$invoice}}" readonly required>
                        </div>
                           
                         </div>
                      </div><hr/>
                      <div class="row">
                         <div class="col-md-3">
                           <div class="form-group ">
                            <a href="#" id="addInput" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> ပစၥည္း အသစ္သြင္းရန္</a>
                            
                        </div>
                         </div>
                         <div class="col-md-offset-6 col-md-3">
                           
                         </div>
                      </div>
                      <div class="row saleItem">
                      <div class="form-group col-md-2 col-sm-2 col-xs-6">
                            <label for="sale_barcode">Bar Code</label>
                            <input type="text" class='form-control sale_barcode' name="sale_barcode[]" value="{{old('sale_barcode')}}" placeholder="Bar Code" required>
                        </div>
                        <div class="form-group col-md-2 col-sm-2 col-xs-6">
                            <label for="sale_itemID">ပစၥည္း အမည္</label>
                            <input type="text" id="name-1" class='form-control sale_itemID'  value="{{old('sale_itemID')}}" readonly="readonly" placeholder="ပစၥည္း အမည္" required>
                            <input type="hidden" id="item-1" class="itemid" name="sale_itemID[]" >
                              
                        </div>
                        <div class="form-group col-md-2 col-sm-2 col-xs-6">
                            <label for="sale_itemqty">ပစၥည္း အေရအတြက္</label>
                            <input type="text" id="stock-1" class='form-control sale_itemqty' id="sale_itemqty" name="sale_itemqty[]" placeholder="ပစၥည္း အေရအတြက္" value="{{old('sale_itemqty')}}" required>
                        </div>
                        <div class="form-group col-md-2 col-sm-2 col-xs-6">
                            <label for="name">ပစၥည္း ေစ်းႏႈန္း</label>
                            <input type="text" id="price-1" class='form-control item_sellprice'  name="sale_price[]" placeholder="ပစၥည္း ေစ်းႏႈန္း" value="{{old('sale_price')}}" readonly="readonly" required>
                        </div>
                        <div class="form-group col-md-2 col-sm-2 col-xs-6">
                            <label for="sale_total">စုစုေပါင္း</label>
                            <input type="text" class='form-control sale_total' name="sale_total[]" id="sale_total" placeholder="စုစုေပါင္း"  readonly="readonly" value="{{old('sale_total')}}" required>
                        </div>
                        <div class="form-group col-md-1 col-sm-1 col-xs-6">
                           
                        </div>
                      </div>
                     
                       <div id="shwInput"></div>
                       <div class="row">
                         <div class="form-group col-md-offset-9 col-md-2 col-sm-2 col-xs-6">
                            <label for="sale_total">စုစုေပါင္း ေစ်းႏႈန္းမ်ား</label>
                            <input type="text" class='form-control' id="all_total" name="sale_alltotal" readonly="readonly" placeholder="စုစုေပါင္း ေစ်းႏႈန္းမ်ား" value="{{old('sale_alltotal')}}" required>
                        </div>
                        <div class="col-md-1"></div>
                       </div>
                       <hr/>
                        <div class="form-group col-md-11 col-sm-11 col-xs-11">
                            <!-- <a href="{{route('sale.receive')}}" target="_black" class="btn btn-info" ><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Receive</a> -->
                            <div class="pull-right">
                            <a href="{{route('sale.index')}}" class="btn btn-default" ><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ဖ်က္ပါ</a>
                            <button type="submit" id="submit" class="btn btn-primary"> <i class="fa fa-save"></i> သိမ္းပါ</button>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </form>
                    
  
  
                  </div>
                </div>
              </div>
            </div>
           
           
         
          </div>
        </div>
@endsection

@section('footerscript')
   <script src="{{ asset('js/select2.min.js') }}"></script>
   <script src="{{ asset('js/moment.js') }}"></script>
   <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
  
   <script type="text/javascript">
     $(document).ready(function(){

      $('#item_supplier').select2();
     });
   </script>

   <script type="text/javascript">
            $(function () {
                $('#datetimepicker').datetimepicker({
                  format: 'YYYY-MM-DD'
                });
            });
        </script>
        <script type="text/javascript">
          $(document).ready(function(){
              var InputsWrapper   = $("#shwInput"); //Input boxes wrapper ID
                var AddButton       = $("#addInput"); //Add button ID

                var x = InputsWrapper.length; //initlal text box count
                var FieldCount=2; //to keep track of text box added

                //on add input button click
                $(AddButton).click(function (e) {
                        //max input box allowed
                        
                            //text box added ncrement
                            //add input box
                            $(InputsWrapper).append(' <div class="row saleItem"><div class="form-group col-md-2 col-sm-2 col-xs-6"><label for="sale_barcode">Bar Code</label><input type="text" class="form-control sale_barcode" name="sale_barcode[]"  placeholder="Bar Code" required></div><div class="form-group col-md-2 col-sm-2 col-xs-6"><label for="sale_itemID">ပစၥည္း အမည္</label><input type="text" id="name-'+FieldCount+'" readonly="readonly"  class="form-control sale_itemID" name=""  placeholder="ပစၥည္း အမည္" required><input type="hidden" id="item-'+FieldCount+'" class="itemid" name="sale_itemID[]" ></div><div class="form-group col-md-2 col-sm-2 col-xs-6 "><label for="sale_itemqty">ပစၥည္း အေရအတြက္</label><input type="text" id="stock-'+FieldCount+'"  class="form-control sale_itemqty" id="sale_itemqty" name="sale_itemqty[]" placeholder="ပစၥည္း အေရအတြက္"  required></div><div class="form-group col-md-2 col-sm-2 col-xs-6"><label for="name">ပစၥည္း ေစ်းႏႈန္း</label><input type="text" class="form-control item_sellprice" id="price-'+FieldCount+'"  name="sale_price[]" placeholder="ပစၥည္း ေစ်းႏႈန္း" readonly="readonly" required></div><div class="form-group col-md-2 col-sm-2 col-xs-6"><label for="sale_total">စုစုေပါင္း</label><input type="text" class="form-control sale_total" readonly="readonly" name="sale_total[]" id="sale_total" placeholder="စုစုေပါင္း"  required></div><div class="form-group col-md-1 col-sm-1 col-xs-6"><a href="#" class="btn btn-danger removeSale" style="margin-top: 24px;"><span class="glyphicon glyphicon-remove " aria-hidden="true"></span></a></div></div>');
                            x++; //text box increment
                             FieldCount++;
                            $("#AddMoreFileId").show();
                            
                            $('AddMoreFileBox').html("Add field");
                            
                            // Delete the "add"-link if there is 3 fields.
                            if(x == 3) {
                                $("#AddMoreFileId").hide();
                              $("#lineBreak").html("<br>");
                            }
                       
                });
          });
        </script>
       
<script type="text/javascript">
  $(document).ready(function(){
        $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });
   
    $('#sale').on('keyup','.sale_barcode',function(){
          var alltotal = 0;
          var count = 0;
           $(".saleItem").each(function(){
            
            //var totalPoints = 0;
            var qty = $(this).find('.sale_itemqty').val();
            var barcode = $(this).find('.sale_barcode').val();
            
         
            var r = "<?php echo route('codeitem'); ?>";
            $.ajax({
              type  :'POST',
              url : r,
              data :{code:barcode},
              dataType: "json",
              success:function(data){
                 
                  var one = "#name-"+count;
                  var two = "#stock-"+count;
                  var three = "#price-"+count;
                  var four = '#item-'+count;
                  if(data.qty > 0){
                    $(one).val(data.name);
                    $(two).val(1);
                    $(three).val(data.price);
                    $(four).val(data.itemid);
                     $('button').prop('disabled', false);
                  }else{
                    alert('sorry this  stock out');
                     $('button').prop('disabled', true);
                    return false;
                  }
                  

                
                  
              }
              
            });
            
            
            var price = $(this).find('.item_sellprice').val();
            var total = qty * price;
             alltotal += +total;
           
            $(this).find('.sale_total').val(total);
            
            count++;
          });
        $('#all_total').val(alltotal);
    });
     $('#sale').on('keyup','.sale_itemqty',function(){
          var alltotal = 0;
          var count = 0;
           $(".saleItem").each(function(){
            
            //var totalPoints = 0;
            var item = $(this).find('.itemid').val();
            
            var qty = $(this).find('.sale_itemqty').val();
           
            var price = $(this).find('.item_sellprice').val();
            var total = qty * price;
             alltotal += +total;

            var r = "<?php echo route('checkqty'); ?>";
            $.ajax({
              type  :'POST',
              url : r,
              data :{id:item,itemqty:qty},
              dataType: "json",
              success:function(data){
                 if(data == 1){
                    $('button').prop('disabled', false);
                 }else{
                   alert('Sorry Item stock out');
                   $('button').prop('disabled', true);
                 }
                  
              }
              
            });

            $(this).find('.sale_total').val(total);
            
            count++;
          });
        $('#all_total').val(alltotal);
    });

    
        $('#sale').on('click','.removeSale',function(e){
            
            $(this).parent('div').parent('div.saleItem').remove();
        });
    
  });
</script>
@stop

