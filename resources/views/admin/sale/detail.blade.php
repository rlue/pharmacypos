@extends('layouts.admin')

@section('content')
    

    <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

                <h3>အေရာင္း စီမံေရး<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>စီမံေရး <small>အေရာင္း</small></h2>
                    <!-- <a href="{{route('sale.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Item</a> -->
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>ပစၥည္း အမည္</th>
                          <th>ပစၥည္း အေရအတြက္</th>
                          <th>ပစၥည္း ေစ်းႏႈန္း</th>
                          <th>စုစုေပါင္း ေစ်းႏႈန္း</th>
                           <th>ရက္စြဲ</th>
                          
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($saledetail as $i)
                        <tr>
                          <td>{{$i->item->item_name}}</td>
                         <td>{{$i->sale_itemqty}}</td>
                         <td>{{$i->sale_price}}</td>
                         <td>{{$i->sale_total}}</td>
                         <td>{{$i->saleitem_date}}</td>
                         
                        
                          
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
					
					
                 
            </div>
          </div>
        </div>
@endsection


