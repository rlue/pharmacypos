@extends('layouts.admin')

@section('content')
    

    <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

                <h3>အေရာင္း စီမံေရး<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>စီမံေရး <small>အေရာင္း</small></h2>
                    <!-- <a href="{{route('sale.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Create Item</a> -->
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="row">
                  <form action="{{route('sale.totalexport')}}" method="post" >
                   {{ csrf_field() }}
                    <div class="col-md-3"> 
                    <label>စရက္</label>
                      <div class='input-group date' id='arrival_Date'>
                            <input type="text" id="fromdate" class='form-control' name="stardate" required="required" placeholder=" ရက္စြဲ"  >
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6"></div>
                    <div class="col-md-3">
                    <label>ဆံုးရက္</label>
                       <div class='input-group date' id='arrival_Date'>
                            <input type="text" required="required" id="todate" class='form-control' name="enddate" placeholder="ရက္စြဲ"  >
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                      
                    
                  </div>
                  <div class="row">
                    <div class="col-md-offset-9 col-md-2">
                      <select name="type" class="form-control">
                          <option value="xls">XLS</option>
                          <option value="xlsx">XLSX</option>
                          <option value="csv">CSV</option>
                         
                      </select>
                    </div>
                    <div class=" col-md-1">

                      <input type="submit" class="btn btn-success pull-right" value="စာရင္းထုတ္">
                    </div>
                  </div>
                </form>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>အေရာင္း Code</th>
                          <th>ေရာင္းသူ</th>
                          <th>ဝယ္သူ</th>
                          <th>စုစုေပါင္း ေစ်းႏႈန္း</th>
                           <th>ရက္စြဲ</th>
                           <th>စာရင္းထုတ္</th>
                           <th>ၾကည့္ရန္</thead>
                          <!-- <th>Edit</th>
                          <th>Delete</th> -->
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($sale as $i)
                        <tr>
                          <td>{{$i->sale_code}}</td>
                         <td>{{$i->sale_seller}}</td>
                         <td>{{$i->sale_buyer}}</td>
                         <td>{{$i->sale_alltotal}}</td>
                         <td>{{$i->sale_date}}</td>
                         <td>
                           <form action="{{ route('sale.itemexport') }}" method="POST"> 
                        {{ csrf_field() }}  
                          <input type="hidden" name="saleitem_code" value="{{$i->sale_code}}">
                        
                         <button type="submit"  class="btn btn-success"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span> စာရင္းထုတ္</button>
                        </form>
                         </td>
                         <td>
                          <form action="{{ route('sale.saledetail') }}" method="POST"> 
                        {{ csrf_field() }}  
                          <input type="hidden" name="saleitem_code" value="{{$i->sale_code}}">
                          <input type="hidden" name="saleitem_date" value="{{$i->sale_date}}">
                         <button type="submit"  class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> အေသးစိတ္ ၾကည့္ရန္</button>
                        </form>
                         </td>
                          <!-- <td><a href="{{route('sale.edit',$i->sale_id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a></td>
                          <td><a href="{{route('sale.delete',$i->sale_id)}}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a></td> -->
                          
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
					
					
                 
            </div>
          </div>
        </div>
@endsection


@section('footerscript')
   <script src="{{ asset('js/moment.js') }}"></script>
   <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
   
  
    <script type="text/javascript">
            $(function () {
                $('#fromdate').datetimepicker({
                  format: 'YYYY-MM-DD'
                });
                $('#todate').datetimepicker({
                  format: 'YYYY-MM-DD'
                });
            });
            
        </script>
   <script type="text/javascript">
   $.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min = parseInt( $('#fromdate').val(), 10 );
        var max = parseInt( $('#todate').val(), 10 );
        var date = parseFloat( data[4] ) || 0; // use data for the age column
 
        if ( ( isNaN( min ) && isNaN( max ) ) ||
             ( isNaN( min ) && date <= max ) ||
             ( min <= date   && isNaN( max ) ) ||
             ( min <= date   && date <= max ) )
        {
            return true;
        }
        return false;
    }
);
     $(document).ready(function(){
      var table =  $('#datatable').DataTable();
    
            
              $('#fromdate, #todate').keyup( function() {
            table.fnDraw();
        } );
              
     });
   </script>
  
      
@stop