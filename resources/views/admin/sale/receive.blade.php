@extends('layouts.admin')

@section('headerstyle')
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@stop
@section('content')
    

   <div class="right_col" role="main">
          <div id="saleitem">
            <div class="page-title">
              <div class="title_left">
                <h3>Sales Invoice</h3>
              </div>

              
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Invoice - {{$input['saleno_code']}}</h2>
                    <h4 class="pull-right">Seller - {{$input['sale_seller']}}</h4>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                      <div class="row">
                         <div class="col-md-12">
                         <span class="pull-right">Date - {{$input['saleitem_date']}}</span>

                           <table class="table table-hover">
                             
                             <tr>
                               <th>Item Name</th>
                               <th>Qty</th>
                               <th>Amount</th>
                             </tr>
                              @for ($i = 0; $i < 2; $i++)
                              <?php $item = DB::table('items')->where('item_id','=',$input['sale_itemID'][$i])->get(); ?>
                              @foreach($item as $m)
                                 <tr>
                               <td>{{$m->item_name}}</td>
                               <td>{{$input['sale_itemqty'][$i]}}</td>
                               <td>{{$input['sale_total'][$i]}}</td>
                             </tr>
                             @endforeach
                              @endfor
                              
                           </table>
                           <span class="pull-right">Total Price- {{$input['sale_alltotal']}}</span>
                         </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
         
         
          </div>
        </div>
@endsection

@section('footerscript')
   <script src="{{ asset('js/select2.min.js') }}"></script>
   <script src="{{ asset('js/moment.js') }}"></script>
   <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
  <script src="{{ asset('js/itemsale.js') }}"></script>
   <script type="text/javascript">
     $(document).ready(function(){

      $('#item_supplier').select2();
     });
   </script>

   <script type="text/javascript">
            $(function () {
                $('#datetimepicker').datetimepicker({
                  format: 'YYYY-MM-DD'
                });
            });
        </script>
        <script type="text/javascript">
          $(document).ready(function(){
              var InputsWrapper   = $("#shwInput"); //Input boxes wrapper ID
                var AddButton       = $("#addInput"); //Add button ID

                var x = InputsWrapper.length; //initlal text box count
                var FieldCount=1; //to keep track of text box added

                //on add input button click
                $(AddButton).click(function (e) {
                        //max input box allowed
                        
                            FieldCount++; //text box added ncrement
                            //add input box
                            $(InputsWrapper).append(' <div class="row saleItem"><div class="form-group col-md-3 col-sm-3 col-xs-6"><label for="itemID">Item Name</label><input type="text" class="form-control" name="itemID"  placeholder="Item Name" required><input type="hidden" class="form-control" name="sale_shop" value="{{ Auth::user()->branch }}" ><input type="hidden" class="form-control" name="sale_seller" value="{{ Auth::user()->id }}" ></div><div class="form-group col-md-3 col-sm-3 col-xs-6 "><label for="sale_itemqty">Item Stock</label><input type="text" class="form-control sale_itemqty" id="sale_itemqty" name="sale_itemqty" placeholder="Item Stock"  required></div><div class="form-group col-md-3 col-sm-3 col-xs-6"><label for="name">Item Price</label><input type="text" class="form-control item_sellprice"  name="item_sellprice" placeholder="Item Price"  required></div><div class="form-group col-md-2 col-sm-2 col-xs-6"><label for="sale_total">Total Price</label><input type="text" class="form-control sale_total" readonly="readonly" name="sale_total" id="sale_total" placeholder="Total Price"  required></div><div class="form-group col-md-1 col-sm-1 col-xs-6"><a href="#" class="btn btn-danger removeSale" style="margin-top: 24px;"><span class="glyphicon glyphicon-remove " aria-hidden="true"></span></a></div></div>');
                            x++; //text box increment
                            
                            $("#AddMoreFileId").show();
                            
                            $('AddMoreFileBox').html("Add field");
                            
                            // Delete the "add"-link if there is 3 fields.
                            if(x == 3) {
                                $("#AddMoreFileId").hide();
                              $("#lineBreak").html("<br>");
                            }
                       
                });
          });
        </script>
        
<script type="text/javascript">
  $(document).ready(function(){
    
   
        $('#sale').on('keyup',function(){
          var alltotal = 0;
       $(".saleItem").each(function(){
        //var totalPoints = 0;
        var qty = $(this).find('.sale_itemqty').val();

        var price = $(this).find('.item_sellprice').val();
        var total = qty * price;
         alltotal += +total;
       
        $(this).find('.sale_total').val(total);
        

      });
        $('#all_total').val(alltotal);
    });

        $('#sale').on('click','.removeSale',function(e){
            
            $(this).parent('div').parent('div.saleItem').remove();
        });
    
  });
</script>
@stop

