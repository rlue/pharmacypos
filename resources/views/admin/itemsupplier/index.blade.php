@extends('layouts.admin')

@section('content')
    

    <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

                <h3>ျဖန္႕ေဝသူ၏ ပစၥည္း စီမံေရး<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>စီမံေရး <small>ျဖန္႕ေဝသူ၏ ပစၥည္း</small></h2>
                    <a href="{{route('supplierItem.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> ပစၥည္း အသစ္ျဖည့္ရန္</a>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <form method="post" action="{{ route('supplierItem.export') }}">
                   {{ csrf_field() }}
                  <div class="row">
                    <div class="col-md-3"> 
                      <select id="dropdown1" name="supplierName" class="form-control">
                         <option value>All</option>
                          @foreach($supplier as $s)
                          <option value="{{ $s->id }}">{{$s->supplier_name}}</option>
                          @endforeach
                      </select>
                    </div>
                    <div class="col-md-6"></div>
                    <div class="col-md-3">
                       <div class='input-group date' id='arrival_Date'>
                            <input type="text" id="dropdown2" class='form-control' name="arrivalDate" placeholder="Arrival Date"  >
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                         </div>
                    </div>
                      
                    
                  </div>
                  <div class="row">
                    <div class="col-md-offset-9 col-md-2">
                      <select name="type" class="form-control">
                          <option value="xls">XLS</option>
                          <option value="xlsx">XLSX</option>
                          <option value="csv">CSV</option>
                         
                      </select>
                    </div>
                    <div class=" col-md-1">

                      <input type="submit" class="btn btn-success pull-right" value="Export">
                    </div>
                  </div>
                </form>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>ပစၥည္းအမည္</th>
                          <th>အေရအတြက္</th>
                          <th>ေစ်းႏႈန္း</th>
                          <th>စုစုေပါင္း ေစ်းႏႈန္း</th>
                          <th>ျဖန္႕ေဝသူ</th>
                          <th>ေရာက္ရွိသည့္ ရက္စြဲ</th>
                          <th>ေငြေပးေခ်သည့္ ရက္စြဲ</th>
                          
                          <th>ျပင္ဆင္ရန္</th>
                          <th>ဖ်က္ရန္</th>
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($supplieritem as $i)
                        <tr>
                          
                         <td>{{$i->itemName}}</td>
                         <td>{{$i->itemQty}}</td>
                         <td>{{$i->itemPrice}}</td>
                         <td>{{$i->priceAmount}}</td>
                         <td>{{$i->supplier_name}}</td>
                         <td>{{$i->arrivalDate}}</td>
                         <td>{{$i->paymentDate}}</td>
                          <td><a href="{{route('supplierItem.edit',$i->supplieritem_id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> ျပင္ဆင္ရန္</a></td>
                          <td><a href="{{route('supplierItem.delete',$i->supplieritem_id)}}" class="btn btn-danger"><i class="fa fa-trash"></i> ဖ်က္ရန္</a></td>
                          
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
					
					
                 
            </div>
          </div>
        </div>
@endsection

@section('footerscript')
   <script src="{{ asset('js/moment.js') }}"></script>
    
   <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
   
  
    <script type="text/javascript">
            $(function () {
                $('#dropdown2').datetimepicker({
                  format: 'YYYY-MM-DD'
                });
            });
            
        </script>
   <script type="text/javascript">
     $(document).ready(function(){
      var table =  $('#datatable').DataTable();
    
              $('#dropdown1').on('change', function () {
                var ss = $("#dropdown1 option:selected").text();
                    if(ss != 'All'){
                      table.columns(4).search(ss).draw();
                    }else{
                      table.columns(4).search($(this).val()).draw();
                    }
                    
                } );
                $('#dropdown2').bind('keyup keydown keypress change blur',function () {
                  //var paydate = $(this).val();
               
                    table.columns(6).search( this.value ).draw();
                });
              
     });
   </script>
  
      
@stop

