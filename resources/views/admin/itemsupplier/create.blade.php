@extends('layouts.admin')

@section('headerstyle')
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@stop
@section('content')
    

   <div class="right_col" role="main">
          <div id="itemsupplier">
           
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ပစၥည္း အသစ္ျဖည့္ရန္ေဖာင္  <small>different form elements</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <form method="post" action="{{ route('supplierItem.store') }}">
                        {{ csrf_field() }}
                      <div class="row">
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                              <label for="name">ျဖန္႕ေဝသူ အမည္</label>
                              <select name="supplierName" class="form-control" id="supplierName">
                                  <option value="0">ကုမၸဏီ ေရြးရန္</option>
                                  @foreach($supplier as $s)
                                    <option value="{{$s->id}}">{{$s->supplier_company}}</option>
                                  @endforeach
                              </select>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="name">ေရာက္ရွိသည့္ ရက္စြဲ</label>
                            <div class='input-group date' id='arrivalDate'>
                            <input type="text"  class='form-control' name="arrivalDate" placeholder="ေရာက္ရွိသည့္ ရက္စြဲ" value="{{old('arrivalDate')}}" required>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                         </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group  col-md-6 col-sm-6 col-xs-12">
                              <label for="name">ေငြေပးေခ်သည့္ ရက္စြဲ</label>
                              <div class='input-group date ' id="paymentDate">
                               <input type="text" class='form-control' name="paymentDate" placeholder="ေငြေပးေခ်သည့္ ရက္စြဲ" value="{{old('paymentDate')}}" required>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                             <input type="hidden" class='form-control' name="store_branch" value="{{ Auth::user()->branch }}" >
                          </div>
                      
                       <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="itemName">ပစၥည္း အမည္</label>
                            <input type="text" class='form-control' name="itemName" placeholder="ပစၥည္း အမည္" value="{{old('itemName')}}" required>
                            
                        </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-4 col-sm-4 col-xs-12">
                              <label for="itemQty">ပစၥည္း အေရအတြက္</label>
                              <input type="text" v-model="itemQty" class='form-control' name="itemQty" placeholder="ပစၥည္း အေရအတြက္" value="{{old('itemQty')}}" required>
                              
                          </div>
                          <div class="form-group col-md-4 col-sm-4 col-xs-12">
                              <label for="itemPrice">ပစၥည္း ေစ်းႏႈန္း</label>
                              <input type="text" v-model="itemPrice" class='form-control' name="itemPrice" placeholder="ပစၥည္း ေစ်းႏႈႏ္း" value="{{old('itemPrice')}}" required>
                              
                          </div>
                         <div class="form-group col-md-4 col-sm-4 col-xs-12">
                              <label for="priceAmount">စုစုေပါင္း ေစ်းႏႈန္း</label>
                              <input type="text"  v-model="total" class='form-control' name="priceAmount" placeholder="စုစုေပါင္း ေစ်းႏႈန္း" value="{{old('priceAmount')}}" required>
                      </div>
                      </div>
                         
                        <div class="form-group">
                            <a href="{{ route('supplierItem.index') }}" class="btn btn-default"><i class="fa fa-close"></i> ဖ်က္ပါ</a>
                            <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> သိမ္းပါ</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

           
         
          </div>
        </div>
@endsection

@section('footerscript')
   <script src="{{ asset('js/select2.min.js') }}"></script>
   <script src="{{ asset('js/moment.js') }}"></script>
   <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
  <script src="{{ asset('js/itemsupplier.js') }}"></script>
   <script type="text/javascript">
     $(document).ready(function(){

      $('#supplierName').select2();
     });
   </script>
   <script type="text/javascript">
            $(function () {
                $('#arrivalDate').datetimepicker({
                  format: 'YYYY-MM-DD'
                });
            });
             $(function () {
                $('#paymentDate').datetimepicker({
                  format: 'YYYY-MM-DD'
                });
            });
        </script>
      
@stop

