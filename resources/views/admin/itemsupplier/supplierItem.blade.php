@extends('layouts.admin')

@section('headerstyle')
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@stop
@section('content')
    

   <div class="right_col" role="main">
          <div id="itemsupplier">
            @{{ message }}
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Item Manageent Form  <small>different form elements</small></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <form method="post" action="{{ route('supplierItem.store') }}">
                        {{ csrf_field() }}
                        <div class="col-md-3">
                          <div class="form-group">
                              <label for="name">Supplier Name</label>
                              <select name="supplier" class="form-control" id="supplierName">
                                  <option value="0">Choose Company</option>
                                  @foreach($supplier as $s)
                                    <option value="{{$s->id}}">{{$s->supplier_company}}</option>
                                  @endforeach
                              </select>
                          </div>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-3">
                          <div class="form-group  ">
                            <label for="name">Supplier Code</label>
                            <input type="text" class='form-control' readonly="readonly" name="item_code" placeholder="Item Code" value="{{$suppliercode}}" required>
                        </div>
                        </div>
                        
                        
                       
                        
                        <div class="form-group col-md-offset-9 col-md-3 col-xs-12">
                            <label for="name">Arrival Date</label>
                            <div class='input-group date' id='datetimepicker'>
                            <input type="text"  class='form-control' name="item_arrivedate" placeholder="Arrival Date" value="{{old('item_arrivedate')}}" required>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                         </div>
                            
                           
                        </div>
                        <div class="form-group col-md-offset-9 col-md-3 col-xs-12">
                              <label for="name">Payment Date</label>
                              <div class='input-group date payment_date'>
                               <input type="text" class='form-control' name="item_expiredate" placeholder="Payment Date" value="{{old('item_expiredate')}}" required>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                              
                              <input type="hidden" class='form-control' name="branch_store" value="{{ Auth::user()->branch }}" >
                          </div>
                      
                       <div class="form-group col-md-3 col-sm-3 col-xs-12">
                            <label for="itemName">Item Name</label>
                            <input type="text" class='form-control' name="itemName" placeholder="Item Name" value="{{old('itemName')}}" required>
                            
                          </div>
                          <div class="form-group col-md-3 col-sm-3 col-xs-12">
                              <label for="itemQty">Item Qty</label>
                              <input type="text" v-model="itemQty" class='form-control' name="itemQty" placeholder="Item Qty" value="{{old('itemQty')}}" required>
                              
                          </div>
                          <div class="form-group col-md-3 col-sm-3 col-xs-12">
                              <label for="itemPrice">Item Price</label>
                              <input type="text" v-model="itemPrice" class='form-control' name="itemPrice" placeholder="Item Price" value="{{old('itemPrice')}}" required>
                              
                          </div>
                         <div class="form-group col-md-3 col-sm-3 col-xs-12">
                              <label for="priceAmount">Amount</label>
                              <input type="text"  v-model="total" class='form-control' name="priceAmount" placeholder="Item Price" value="{{old('priceAmount')}}" required>
                              
                        </div>
                     
                         
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">

                            <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> Submit</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

           
         
          </div>
        </div>
@endsection

@section('footerscript')
   <script src="{{ asset('js/select2.min.js') }}"></script>
   <script src="{{ asset('js/moment.js') }}"></script>
   <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/vue.resource/0.9.3/vue-resource.min.js"></script>
  <script src="{{ asset('js/itemsupplier.js') }}"></script>
   <script type="text/javascript">
     $(document).ready(function(){

      $('#supplier_company').select2();
     });
   </script>
   <script type="text/javascript">
            $(function () {
                $('#datetimepicker').datetimepicker({
                  format: 'YYYY-MM-DD'
                });
            });
             $(function () {
                $('.payment_date').datetimepicker({
                  format: 'YYYY-MM-DD'
                });
            });
        </script>
       <!--  <script type="text/javascript">
          function addItem()
          {
           
           document.getElementById('wrapper').innerHTML += '<div class="form-group col-md-3 col-sm-3 col-xs-12"><label for="name">Item Name</label><input type="text"  class="form-control" name="itemName[]" placeholder="Item Name" required></div><div class="form-group col-md-3 col-sm-3 col-xs-12"><label for="name">Item Qty</label><input type="text"  v-model="itemQty" class="form-control" name="itemQty[]" placeholder="Item Qty" required></div><div class="form-group col-md-3 col-sm-3 col-xs-12"><label for="name">Item Price</label><input type="text" id="itemPrice" class="form-control" name="itemPrice[]" placeholder="Item Price" required></div><div class="form-group col-md-3 col-sm-3 col-xs-12"><label for="name">Amount</label><input type="text" id="itemPrice" class="form-control" name="itemAmount[]" placeholder="Item Price" required></div>';
          }
         
        </script> -->
@stop

