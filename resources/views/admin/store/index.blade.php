@extends('layouts.admin')

@section('content')
    

    <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">

                <h3>ဆိုင္ခြဲမ်ား<small></small></h3>
              </div>

              <div class="title_right">
              @if (Session::has('message'))
                            <div class="alert alert-dismissible alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('message') }}
                            </div>
                            @endif
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <!-- <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span> -->
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <h2>စီမံေရး <small>ဆိုင္ခြဲမ်ား</small></h2>
                    <a href="{{route('stores.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> ဆိုင္ခြဲအသစ္ ျဖည့္ရန္</a>
                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>အမည္</th>
                          
                          <th>ျပင္ဆင္ရန္</th>
                          <th>ဖ်က္ရန္</th>
                        </tr>
                      </thead>


                      <tbody>
                      @foreach($store as $s)
                        <tr>
                          <td>{{$s->store_name}}</td>
                         
                          <td><a href="{{route('stores.edit',$s->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i> ျပင္ဆင္ရန္</a></td>
                          <td><a href="{{route('stores.delete',$s->id)}}" class="btn btn-danger"><i class="fa fa-trash"></i> ဖ်က္ရန္</a></td>
                          
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              
					
					
                 
            </div>
          </div>
        </div>
@endsection


