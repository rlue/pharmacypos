@extends('layouts.admin')

@section('content')
    

   <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ဝန္ထမ္း စီမံေရး <small>different form elements</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                     @if (count($errors) > 0)
       <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
                    <form method="post" action="{{route('users.update',$user->id)}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">ဝန္ထမ္း အမည္ *</label>
                        <input type="text" class='form-control' name="name" value="{{$user->name}}" placeholder="ဝန္ထမ္း အမည္">
                    </div>
                    <div class="form-group">
                        <label for="name">Email *</label>
                        <input type="email" class='form-control' name="email" value="{{$user->email}}" placeholder="User Email">
                    </div>
                    @if($user->is_super != 1)
                     <div class="form-group">
              
                            @foreach($store as $s)
                              @if($s->id == $user->branch)
                               <label class="radio-inline">
                            <input type="radio" name="branch" id="inlineRadio2" checked="checked" value="{{$s->id}}"> {{$s->store_name}}
                              @else
                               <label class="radio-inline">
                            <input type="radio" name="branch" id="inlineRadio2" value="{{$s->id}}"> {{$s->store_name}}
                              @endif
                           
                          </label>
                          @endforeach
                        </div>
                      @endif  
                    <div class="form-group">
                        <label for="name">လၽိဳ႕ဝွက္နံပါတ္ *</label>
                        <input type="password" class='form-control' name="password" placeholder="လၽိဳ႕ဝွက္နံပါတ္">
                    </div>
                    <div class="form-group">
                        <label for="name">အတည္ျပဳ လၽိဳ႕ဝွက္နံပါတ္ *</label>
                        <input type="password" class='form-control' name="password_confirmation" placeholder="အတည္ျပဳ လၽိဳ႕ဝွက္နံပါတ္">
                    </div>
                  
                    <form action="{{ route('users.store', $user->id) }}"
                >
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                    <a href="{{ route('users.index') }}" class="btn btn-default"><i class="fa fa-close"></i> ဖ်က္ပါ</a>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> သိမ္းပါ</button>
                </form>
                </form>
                  </div>
                </div>
              </div>
            </div>

           
         
          </div>
        </div>
@endsection


