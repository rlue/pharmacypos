@extends('layouts.admin')
@section('headerstyle')
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@stop
@section('content')
    

   <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ပစၥည္း ျပင္ဆင္ရန္ေဖာင္ <small>different form elements</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                     @if (count($errors) > 0)
       <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
                    <form method="post" action="{{route('item.update',$item->item_id)}}">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                            <label for="name">ပစၥည္း အမည္</label>
                            <input type="text" class='form-control' name="item_name" value="{{$item->item_name}}" placeholder="ပစၥည္း အမည္" required>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-6 ">
                            <label for="name">ပစၥည္း Code</label>
                            <input type="text" class='form-control' name="item_code" placeholder="ပစၥည္း Code" value="{{$item->item_code}}" required>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-6">
                            <label for="name">ေရာင္းေစ်း</label>
                            <input type="text" class='form-control' name="item_sellprice" placeholder="ေရာင္းေစ်း" value="{{$item->item_sellprice}}" required>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-6">
                            <label for="name">ဝယ္ေစ်း</label>
                            <input type="text" class='form-control' name="item_buyprice" placeholder="ဝယ္ေစ်း" value="{{$item->item_buyprice}}" required>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-6 ">
                            <label for="name">အေရအတြက္</label>
                            <input type="text" class='form-control' name="item_stock" placeholder="အေရအတြက္" value="{{$item->item_stock}}" required>
                        </div>
                         <div class="form-group col-md-6 col-sm-6 col-xs-6 ">
                            <label for="bar_code">Bar Code</label>
                            <input type="text" class='form-control' name="bar_code" placeholder="Bar Code" value="{{$item->bar_code}}" required>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-6">
                            <label for="name">ကုမၸဏီ</label>
                            <select class='form-control' id="item_supplier" name="item_supplier">
                                <option value="0">Choose..</option>
                                @foreach($supplier as $s)
                                  @if($s->id == $item->item_supplier)
                                    <option value="{{$s->id}}" selected="selected">{{$s->supplier_company}}</option>
                                  @else
                                    <option value="{{$s->id}}">{{$s->supplier_company}}</option>
                                  @endif
                                  
                                @endforeach
                            </select>
                            
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="name">ကုန္ဆံုးရက္</label>
                            <input type="text" id="datetimepicker" class='form-control' name="item_expiredate" placeholder="ကုန္ဆံုးရက္" value="{{$item->item_expiredate}}" required>
                            <input type="hidden" class='form-control' name="branch_store" value="{{ Auth::user()->branch }}" >
                        </div>
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <form action="{{ route('item.store', $item->item_id) }}"
                >
                    {{ csrf_field() }}
                    {{ method_field("patch") }}
                    <a href="{{ route('item.index') }}" class="btn btn-default"><i class="fa fa-close"></i> ဖ်က္ပါ</a>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> သိမ္းပါ</button>
                    </div>
                </form>
                </form>
                  </div>
                </div>
              </div>
            </div>

           
         
          </div>
        </div>
@endsection

@section('footerscript')
   <script src="{{ asset('js/select2.min.js') }}"></script>
   <script src="{{ asset('js/moment.js') }}"></script>
   <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
   <script type="text/javascript">
     $(document).ready(function(){

      $('#item_supplier').select2();
     });
   </script>
   <script type="text/javascript">
            $(function () {
                $('#datetimepicker').datetimepicker({
                  format: 'YYYY-MM-DD'
                });
            });
        </script>
@stop
