@extends('layouts.admin')

@section('content')
    
   <div class="right_col" role="main">
          <div class="">
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>ျဖန္႕ေဝသူ အသစ္ျဖည့္ရန္ေဖာင္ <small>different form elements</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                     @if (count($errors) > 0)
       <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
                    <form method="post" action="{{ route('supplier.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group col-md-6 col-sm-6 col-xs-12" >
                            <label for="supplier_name">အမည္</label>
                            <input type="text" class='form-control' name="supplier_name" placeholder="အမည္္" required>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="supplier_company">ကုပဏီ အမည္</label>
                            <input type="text" class='form-control' name="supplier_company" placeholder="ကုပဏီ အမည္" required>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="supplier_phone">ဖုန္းနံပါတ္</label>
                            <input type="text" class='form-control' name="supplier_phone" placeholder="ဖုန္းနံပါတ္" required>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="supplier_email">Email</label>
                            <input type="email" class='form-control' name="supplier_email" placeholder="Email">
                        </div>
                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                            <label for="supplier_address">လိပ္စာ</label>
                            <textarea class="form-control" name="supplier_address" required></textarea>
                        </div>
                       
                        <div class="form-group">
                            <a href="{{ route('supplier.index') }}" class="btn btn-default"><i class="fa fa-close"></i> ဖ်က္ပါ</a>
                            <button type="submit" class="btn btn-primary"> <i class="fa fa-save"></i> သိမ္းပါ</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

           
         
          </div>
        </div>
@endsection


