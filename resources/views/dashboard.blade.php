@extends('layouts.admin')

@section('headerstyle')
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
@stop
@section('content')
    <div class="right_col" role="main">
     
          <div class="">
            <div class="row top_tiles" style="margin: 10px 0;">
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
              <div class="all_price">
                <h3>ယေန႕ ေရာင္းေငြ</h3><hr/>
                <h2>{{$today}} က်ပ္</h2>
               
              </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
              <div class="all_price">
                <h3> စုစုေပါင္းအျမတ္ေငြ</h3><hr/>
                <h2> {{$total}} က်ပ္</h2>
               
                  </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
              <div class="all_price">
                <h3>ထြက္ေငြ စုစုေပါင္း</h3><hr/>
                <h2>{{$supplierpay}} က်ပ္</h2>
               
                  </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-6 tile">
              <div class="all_price">
                <h3>စုစုေပါင္းအေရာင္း</h3><hr/>
                <h2>{{$sellprice}} က်ပ္</h2>
               
                  </div>
              </div>
            </div>
            <br />

             <?php $i=1; ?>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="supplier_pay">
                  <h3>ေပးေခ်ရမည့္ သူမ်ား</h3><hr/>
                  <table class="table table-bordered">
                    <tr>
                      <th>စဥ္</th>
                      <th>အမည္</th>
                      <th>ရက္စြဲ</th>
                    </tr>
                    @foreach($payment as $pay)
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$pay->supplier_name}}</td>
                      <td>{{$pay->paymentDate}}</td>
                     
                    </tr>
                    @endforeach
                  </table>
                  </div>
              </div>
              <?php $no=1; ?>
              <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="item_expire">
                  <h3>ရက္လြန္ၿပီး ပစၥည္းမ်ား</h3><hr/>
                  <table class="table table-bordered">
                    <tr>
                      <th>စဥ္</th>
                      <th>အမည္</th>
                      <th>ရက္စြဲ</th>
                      <th>ျဖန္႕ေ၀သူ</th>
                    </tr>
                   
                    @foreach($expireitem as $item)
                    <tr>
                      <td>{{$no++}}</td>
                      <td>{{$item->item_name}}</td>
                      <td>{{$item->item_expiredate}}</td>
                      <td>{{$item->supplier_name}}</td>
                    </tr>
                    @endforeach
                  </table>
                  </div>
              </div>
            </div>


            
          </div>
        </div>

  
@endsection

@section('footerscript')
  
@stop

