<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Store;
use Session;
use DB;
class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware("auth");
        $this->middleware("superauth");
    }
    public function index()
    {
        //
        
        $store = Store::get();
       
        return view('admin.store.index',compact('store'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.store.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

         $data = [
        'store_name'      => $request->input("store_name"),
        ];

        $stre = Store::create($data);
       Session::flash('message', 'You have successfully Insert Store.');
        return redirect()->route("stores.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $store = Store::FindOrFail($id);
       
        return view('admin.store.edit',compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $store_name  = $request->input("store_name");
        
        
       
             $userupdate = ['store_name' => $store_name];
       
       
       DB::table('stores')
            ->where('id', $id)
            ->update($userupdate);
           

        Session::flash('message', 'You have successfully updated Stre.');
        return redirect()->route('stores.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         Store::destroy($id);
       
        Session::flash('message', 'You have successfully deleted store.');
        return redirect()->route("stores.index"); 
    }
}
