<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Supplier;
use DB;
use Session;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $supplier = Supplier::get();
       
        return view('admin.supplier.index',compact('supplier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $data = [
        'supplier_name'      => $request->input("supplier_name"),
        'supplier_company'     => $request->input('supplier_company'),
        'supplier_phone'     => $request->input('supplier_phone'),
        'supplier_email'     => $request->input('supplier_email'),
        'supplier_address'     => $request->input('supplier_address'),
        
        
        ];
        
        $user = Supplier::create($data);
       Session::flash('message', 'You have successfully Insert Supplier.');
        return redirect()->route("supplier.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $supplier = Supplier::FindOrFail($id);
      
        return view('admin.supplier.edit',compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $data = [
        'supplier_name'      => $request->input("supplier_name"),
        'supplier_company'     => $request->input('supplier_company'),
        'supplier_phone'     => $request->input('supplier_phone'),
        'supplier_email'     => $request->input('supplier_email'),
        'supplier_address'     => $request->input('supplier_address'),
        
        
        ];
         DB::table('suppliers')
            ->where('id', $id)
            ->update($data);
           

        Session::flash('message', 'You have successfully updated Supplier.');
        return redirect()->route('supplier.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Supplier::destroy($id);
       
        Session::flash('message', 'You have successfully deleted Supplier.');
        return redirect()->route("supplier.index"); 
    }
}
