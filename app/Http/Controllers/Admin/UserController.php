<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use DB;
use Session;
use App\Store;
use Auth;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware("auth");
        $this->middleware("superauth");
    }

    public function index()
    {
        //
        $user = User::get();
       
        return view('admin.user.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $store = Store::get();
        return view('admin.user.create',compact('store'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        //
        $data = [
        'name'      => $request->input("name"),
        'email'     => $request->input('email'),
        'password'  => bcrypt($request->input('password')),
        'is_super' => 0,
        'branch' => $request->input('branch'),
        
        ];
        
        $user = User::create($data);
       Session::flash('message', 'You have successfully Insert User.');
        return redirect()->route("users.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         
        $user = User::FindOrFail($id);
       $store = Store::get();
        return view('admin.user.edit',compact('user','store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser  $request, $id)
    {
        //

        $name  = $request->input("name");
        $email     = $request->input('email');
        $password  = $request->input('password');
        $supper = $request->input('supper');
        if($supper == 1){
            $branch = 0;
        }else{
            $branch = $request->input('branch');
        }
        
        
        if(isset($password) && !empty($password)){
            $userupdate = ['name' => $name,'email' => $email,'branch' => $branch,'password'=>bcrypt($password)];

        }else{
             $userupdate = ['name' => $name,'email' => $email,'branch' => $branch];
        }
       dd($userupdate);
       DB::table('users')
            ->where('id', $id)
            ->update($userupdate);
           

        Session::flash('message', 'You have successfully updated User.');
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        User::destroy($id);
       
        Session::flash('message', 'You have successfully deleted User.');
        return redirect()->route("users.index"); 
    }
}
