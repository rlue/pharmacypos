<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
class DashboardController extends Controller
{
    //

    public function index()
    {

    	$branch =  Auth::user()->branch;
    	$sale = DB::table('sales')
    	 ->leftJoin('sale_items', 'sale_items.saleno_code', '=', 'sales.sale_code')
    	 ->leftJoin('items', 'items.item_id', '=', 'sale_items.sale_itemID')
    	->where('sale_shop','=',$branch)
    	->get();
    	//total sale price
    	$total=0;
    	$fix = 0;
    	$sellprice = 0;
	    $buyprice = 0;
	    
    	foreach($sale as $s){

    		$sellprice += $s->item_sellprice * $s->sale_itemqty;
	    	$buyprice += $s->item_buyprice * $s->sale_itemqty;	
    		
    	}
    	$total = $sellprice - $buyprice;
    	//today sale price
    	$now =  date('Y-m-d');
    	$paydate = date('Y-m-d', strtotime("+1 days"));;
    	
    	$totalsale = DB::table('sales')->where('sale_shop','=',$branch)->where('sale_date','=',$now)->get();
    	$today=0;
    	foreach($totalsale as $t){
    		$today += $t->sale_alltotal;
    	}
    	//pay supplier total
    	$supplier = DB::table('supplier_items')->where('store_branch','=',$branch)->get();
    	$supplierpay = 0;
    	foreach ($supplier as $pay) {
    		$supplierpay += $pay->priceAmount;
    		# code...
    	}
    	// expire item
    	$expireitem = DB::table('items')
    	->leftJoin('suppliers', 'suppliers.id', '=', 'items.item_supplier')
    	->where('item_expiredate','<',$now)
    	->where('branch_store','=',$branch)
    	->get();

    	//expire suppliery payment
    	$payment = DB::table('supplier_items')
    	->leftJoin('suppliers', 'suppliers.id', '=', 'supplier_items.supplierName')
    	->where('store_branch','=',$branch)
    	->where('paymentDate','=',$paydate)
    	->get();

    	return view('dashboard',compact('total','today','sellprice','supplierpay','expireitem','payment'));
    }
}
