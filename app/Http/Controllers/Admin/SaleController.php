<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sale;
use App\Item;
use App\SaleItem;
use App\Supplier;
use Response;
use DB;
use Session;
use Auth;
use Excel;

use App;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $branch =  Auth::user()->branch;
        //dd($branch);
        $sale = Sale::where('sale_shop','=',$branch)->get();
       
        return view('admin.sale.index',compact('sale'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user = Auth::user();

        $saleno = DB::table('sales')->where('sale_shop','=',$user->branch)->get();
        $code =[];
        foreach($saleno as $s)
        {
            $code[] = $s->sale_code; 
        }
        $codeno = count($code);
        $numbercde = $codeno + 1;
        $invoice ="IVSH".$user->branch.'-'.$numbercde;
        
         return view('admin.sale.create',compact('invoice'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $itemid = $request->input('sale_itemID');
         $itemqty = $request->input('sale_itemqty');
       

        for ($x = 0; $x < count($itemid); $x++) {
            $sale = Item::where('item_id',$itemid[$x])
             ->decrement('item_stock',$itemqty[$x]);
        }
        
         $input = Input::all();

         $condition = $input['sale_itemID'];
         foreach ($condition as $key => $condition) {
             $student = new SaleItem;
             $student->sale_itemID = $input['sale_itemID'][$key];
            $student->sale_itemqty = $input['sale_itemqty'][$key];
             $student->sale_price = $input['sale_price'][$key];
            $student->sale_total = $input['sale_total'][$key];
             $student->saleno_code = $input['saleno_code'];
             $student->saleitem_shop = $input['saleitem_shop'];
             $student->saleitem_date = $input['saleitem_date'];
             $student->save();
         }

        $data = [
         'sale_code'      => $request->input("saleno_code"),
         'sale_seller'     => $request->input('sale_seller'),
         'sale_buyer'     => $request->input('sale_buyer'),
         'sale_alltotal'    => $request->input('sale_alltotal'),
         'sale_shop'     => $request->input('saleitem_shop'),
         'sale_date'     => $request->input('saleitem_date'),
        ];
        $sale  = Sale::create($data);
         $saleno = $request->input('saleno_code');
       
        return redirect()->route("sale.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function saledetail(Request $request){

        $code = $request->input('saleitem_code');
        $saledate = $request->input('saleitem_date');
        $saledetail = SaleItem::with('item')->where('saleno_code','=',$code)->where('saleitem_date','=',$saledate)->get();

         return view('admin.sale.detail',compact('saledetail'));
    }
    public function CodeItem(Request $request)
    {
            $code = $request->input('code');

            $itemcode = DB::table('items')->where('bar_code','=',$code)->get();
            foreach($itemcode as $code){
                $data['itemid'] = $code->item_id;
                $data['name'] = $code->item_name;
                $data['qty'] = $code->item_stock;
                $data['price'] = $code->item_sellprice;
            }
            return Response::json($data);
    }

    public function checkqty(Request $request)
    {
        $item = $request->input('id');
        $qty = $request->input('itemqty');
        $itemcode = Item::FindOrFail($item);
        $count =$itemcode['item_stock'];
        if($count > $qty){
            $data = 1;

        }else{
            $data = 0;
        }
        return Response::json($data);
    }
   
    public function totalExport(Request $request)
    {
            $startdate = $request->input('stardate');
            $enddate = $request->input('enddate');
            $type = $request->input('type');

             $branch =  Auth::user()->branch;

             $total = DB::table('sales')
             ->where('sale_shop','=',$branch)
             ->whereBetween('sale_date',[$startdate,$enddate])
             ->get();
             $i=1;
             $totals = 0;
            $alltotal[0] = ['Sale Report','','','','',''];
             $alltotal[] = ['From Date',$startdate,'','','To Date', $enddate];
             $alltotal[] = ['NO','Item Code','Seller','Buyer','Date','Total Price'];
             foreach($total as $t){
                    $alltotal[] = [
                        'count' => $i++,
                        'sale_code' =>$t->sale_code,
                        'sale_seller' => $t->sale_seller,
                        'sale_buyer' =>$t->sale_buyer,
                        'sale_date' => $t->sale_date,
                        'sale_alltotal' =>$t->sale_alltotal
                    ];
                    $totals += $t->sale_alltotal;
             }
            
            return Excel::create('Report', function($excel) use ($alltotal,$totals) {
                $excel->sheet('Sale Total Report', function($sheet) use ($alltotal,$totals)
                {
                    $sheet->fromArray($alltotal,null,'A1',false,false,false);
                    $sheet->mergeCells('A1:E1');
                    $sheet->setHeight(1, 30);
                    $sheet->setHeight(2, 25);
                    $sheet->setHeight(3, 25);
                    $sheet->row(1, function($row) {
                        $row->setFontSize(14);

                        // Set font weight to bold
                        $row->setFontWeight('bold');
                       

                    });
                    $sheet->row(2, function($row) {

                        $row->setFontSize(12);

                        // Set font weight to bold
                        $row->setFontWeight('bold');

                    });
                    $sheet->row(3, function($row) {
                        $row->setFontWeight('bold');
                        // call cell manipulation methods
                        $row->setBackground('#ebebeb');

                    });
                    $sheet->appendRow(['','','',' ',' ','']);
                    $sheet->appendRow(['','','',' ','Total Price', $totals]);
                });
            })->download($type);
        
    }

    public function SaleExport(Request $request)
    {
        $code =$request->input('saleitem_code');
        $saletotal = DB::table('sales')
        ->where('sale_code','=',$code)
        ->get();
            $saleall[0] = ['Sale Invoice','','','','',''];
        foreach($saletotal as $sale){
            $saleall[] = ['Date',$sale->sale_date,'','','',''];
            $saleall[] = ['Seller',$sale->sale_seller,' ','Buyer',$sale->sale_buyer];
            $total =$sale->sale_alltotal;
           
           
        }
        $saleall[] = ['NO','Item Code','Item Name','Item Qty','Item Price','Total'];
        $itemsale = DB::table('sale_items')
        ->leftJoin('items', 'sale_items.sale_itemID', '=', 'items.item_id')
        ->where('saleno_code','=',$code)
        ->get();
        $i =1;
            foreach($itemsale as $item){
                $saleall[] = [
                'count' => $i++,
                'item_code' =>$item->saleno_code,
                'item_name' =>$item->item_name,
                'item_qty' =>$item->sale_itemqty,
                'sale_price' =>$item->sale_price,
                'sale_total' =>$item->sale_total
                ];
            }
           
        return Excel::create('invoice', function($excel) use ($saleall,$total) {
            $excel->sheet('mySheet', function($sheet) use ($saleall,$total)
            {
                $sheet->fromArray($saleall,null,'A1',false,false,false);
                $sheet->setHeight(1, 20);
                $sheet->setHeight(2, 20);
                $sheet->setHeight(3, 20);
                 $sheet->setHeight(4, 20);
                 $sheet->row(1, function($row) {
                        $row->setFontSize(14);

                        // Set font weight to bold
                        $row->setFontWeight('bold');
                       

                    });
                    $sheet->row(3, function($row) {

                        $row->setFontSize(12);

                        // Set font weight to bold
                        $row->setFontWeight('bold');

                    });
                    $sheet->row(4, function($row) {
                        $row->setFontWeight('bold');
                        // call cell manipulation methods
                        $row->setBackground('#ebebeb');

                    });
                    $sheet->appendRow(['',' ','','','','']);
                    $sheet->appendRow(['',' ','','','Total Price', $total]);
            });
        })->download('xls');
    }

}
