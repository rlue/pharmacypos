<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Supplier;
use App\SupplierItem;
use DB;
use Session;
use Excel;
use Auth;
class SupplierItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $branch =  Auth::user()->branch;
        $supplieritem = DB::table('supplier_items')
            ->leftJoin('suppliers', 'suppliers.id', '=', 'supplier_items.supplierName')
            ->where('store_branch','=',$branch)
            ->get();
       $supplier = Supplier::get();
        return view('admin.itemsupplier.index',compact('supplieritem','supplier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $supplier = Supplier::get();
        
        return view('admin.itemsupplier.create',compact('supplier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $data = [
        'supplierName'      => $request->input("supplierName"),
        'itemName'     => $request->input('itemName'),
        'itemQty'     => $request->input('itemQty'),
        'itemPrice'     => $request->input('itemPrice'),
        'priceAmount'     => $request->input('priceAmount'),
        'arrivalDate'     => $request->input('arrivalDate'),
        'paymentDate'     => $request->input('paymentDate'),
        'store_branch'     => $request->input('store_branch'),
       
        ];
        
        $item = SupplierItem::create($data);
       Session::flash('message', 'You have successfully Insert Supplier Item.');
        return redirect()->route("supplierItem.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $supplieritem = SupplierItem::FindOrFail($id);
        $supplier = Supplier::get();
        return view('admin.itemsupplier.edit',compact('supplieritem','supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = [
        'supplierName'      => $request->input("supplierName"),
        'itemName'     => $request->input('itemName'),
        'itemQty'     => $request->input('itemQty'),
        'itemPrice'     => $request->input('itemPrice'),
        'priceAmount'     => $request->input('priceAmount'),
        'arrivalDate'     => $request->input('arrivalDate'),
        'paymentDate'     => $request->input('paymentDate'),
        'store_branch'     => $request->input('store_branch'),
       
        ];
         DB::table('supplier_items')
            ->where('supplieritem_id', $id)
            ->update($data);
           

        Session::flash('message', 'You have successfully updated Stre.');
        return redirect()->route('supplierItem.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        SupplierItem::destroy($id);
       
        Session::flash('message', 'You have successfully deleted Supplier Item.');
        return redirect()->route("supplierItem.index"); 
    }

    public function downloadExcelFile(Request $request){

        $supplier = $request->input('supplierName');
        $date = $request->input('arrivalDate');

        $type = $request->input('type');
        if(!empty($supplier) && !empty($date)){

            $data = SupplierItem::where('supplierName','=',$supplier )->where('paymentDate','=',$date)->get()->toArray();
        }else if(!empty($supplier) || !empty($date)){
            $data = SupplierItem::leftJoin('suppliers', 'suppliers.id', '=', 'supplier_items.supplierName')
            ->Where('supplierName','=',$supplier)
            ->orWhere('paymentDate','=',$date)
            ->select('itemName','itemQty','itemPrice','priceAmount','supplier_name')
            ->get()->toArray();
        }else{
             $data = SupplierItem::leftJoin('suppliers', 'suppliers.id', '=', 'supplier_items.supplierName')
            ->select('itemName','itemQty','itemPrice','priceAmount','supplier_name')
            ->get()->toArray();

            
        }
       $total = 0;
        foreach($data as $d){
                $total += $d['priceAmount'];
            }
        return Excel::create('supplierItem', function($excel) use ($data,$total) {
            $excel->sheet('mySheet', function($sheet) use ($data,$total)
            {
                $sheet->fromArray($data);
                $sheet->appendRow(['Total Price',' ',' ', $total,]);
            });
        })->download($type);
    }    
}
