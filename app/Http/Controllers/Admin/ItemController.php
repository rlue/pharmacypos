<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use App\Http\Requests\StoreItem;
use App\Supplier;
use DB;
use Auth;
use Session;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $branch =  Auth::user()->branch;
        $item = DB::table('items')
            ->leftJoin('suppliers', 'suppliers.id', '=', 'items.item_supplier')
            ->where('branch_store','=',$branch)
            ->get();
       
        return view('admin.item.index',compact('item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $supplier = Supplier::get();
        return view('admin.item.create',compact('supplier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $data = [
        'item_name'      => $request->input("item_name"),
        'item_code'     => $request->input('item_code'),
        'item_sellprice'     => $request->input('item_sellprice'),
        'item_buyprice'     => $request->input('item_buyprice'),
        'item_stock'     => $request->input('item_stock'),
        'item_supplier'     => $request->input('item_supplier'),
        'item_expiredate'     => $request->input('item_expiredate'),
        'branch_store'     => $request->input('branch_store'),
        'bar_code'     => $request->input('bar_code'),
        ];
        
        $item = Item::create($data);
       Session::flash('message', 'You have successfully Insert Item.');
        return redirect()->route("item.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $item = Item::FindOrFail($id);
        $supplier = Supplier::get();
        return view('admin.item.edit',compact('item','supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
         $data = [
        'item_name'      => $request->input("item_name"),
        'item_code'     => $request->input('item_code'),
        'item_sellprice'     => $request->input('item_sellprice'),
        'item_buyprice'     => $request->input('item_buyprice'),
        'item_stock'     => $request->input('item_stock'),
        'item_supplier'     => $request->input('item_supplier'),
        'item_expiredate'     => $request->input('item_expiredate'),
        'branch_store'     => $request->input('branch_store'),
        'bar_code'     => $request->input('bar_code'),
        ];

        DB::table('items')
            ->where('item_id', $id)
            ->update($data);
           

        Session::flash('message', 'You have successfully updated Item.');
        return redirect()->route('item.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
         Item::destroy($id);
       
        Session::flash('message', 'You have successfully deleted Item.');
        return redirect()->route("item.index"); 
    }


    public function supplierItem()
    {
        $supplier = Supplier::get();
        return view('admin.supplierItem',compact('supplier'));
    }
}
