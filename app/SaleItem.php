<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Item;
class SaleItem extends Model
{
    //

    protected $fillable = [
        'sale_itemID','sale_itemqty','sale_price','sale_total','saleno_code','saleitem_shop','saleitem_date'
    ];

    protected $primaryKey = 'saleitem_id';

     public function Item()
    {
        return $this->belongsTo(Item::class,'sale_itemID');
    }

    
}
