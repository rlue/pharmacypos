<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Item;
use App\User;
class Sale extends Model
{
    //
    protected $fillable = [
        'sale_code','sale_seller','sale_buyer','sale_alltotal','sale_shop','sale_date'
    ];
    protected $primaryKey = 'sale_id';

    public function Item()
    {
        return $this->belongsTo(Item::class,'itemID');
    }

     public function User()
    {
        return $this->belongsTo(User::class,'sale_seller');
    }
}
