<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //
    protected $fillable = [
        'supplier_name','supplier_company','supplier_phone','supplier_email','supplier_address'
    ];
}
