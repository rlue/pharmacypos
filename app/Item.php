<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    protected $fillable = [
        'item_name','item_code','item_sellprice','item_buyprice','item_stock','item_supplier','item_expiredate','branch_store','bar_code'
    ];
    protected $primaryKey = 'item_id';
}
