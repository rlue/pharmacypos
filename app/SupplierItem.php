<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Supplier;

class SupplierItem extends Model
{
    //

    protected $fillable = [
        'supplierName','itemName','itemQty','itemPrice','priceAmount','arrivalDate','paymentDate','store_branch'
    ];
    protected $primaryKey = 'supplieritem_id';

     public function Supplier()
    {
        return $this->belongsTo(Supplier::class,'supplierName');
    }
}
